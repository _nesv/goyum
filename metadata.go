package goyum

import "encoding/xml"

type RepoMetadata struct {
	XMLName  xml.Name `xml:"repomd"`
	Revision int      `xml:"revision"`
	Data     []Data   `xml:"data"`
}

type Data struct {
	XMLName      xml.Name     `xml:"data"`
	Type         string       `xml:"type,attr"`
	Checksum     DataChecksum `xml:"checksum"`
	Timestamp    int64        `xml:"timestamp"`
	Size         int          `xml:"size"`
	OpenSize     int          `xml:"open-size"`
	OpenChecksum DataChecksum `xml:"open-checksum"`
	Location     DataLocation `xml:"location"`
}

type DataChecksum struct {
	XMLName  xml.Name
	Type     string `xml:"type,attr"`
	Checksum string `xml:",chardata"`
}

type DataLocation struct {
	XMLName xml.Name
	Href    string `xml:"href,attr"`
}
