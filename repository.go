package goyum

import (
	yumfl "bitbucket.org/_nesv/goyum/repodata/filelists"
	yumother "bitbucket.org/_nesv/goyum/repodata/other"
	yumprimary "bitbucket.org/_nesv/goyum/repodata/primary"
	"errors"
	"os"
	"path/filepath"
)

type Repository struct {
	Metadata RepoMetadata
	path     string
}

func LoadRepository(path string) (repo *Repository, err error) {
	var repodataPath string = filepath.Join(path, "repodata")

	var fi os.FileInfo
	fi, err = os.Stat(repodataPath)
	if err != nil {
		return
	} else if !fi.IsDir() {
		err = errors.New(repodataPath + " is not a directory")
		return
	}

	repomd := filepath.Join(repodataPath, "repomd.xml")
	if _, err = os.Stat(repomd); err != nil {
		return
	}

	var md RepoMetadata
	if err = loadXML(repomd, &md); err != nil {
		return
	}

	// var other yumother.Otherdata
	// var primary yumprimary.Primary
	// var fl yumfl.Filelists
	// for _, data := range md.Data {
	// 	pth := filepath.Join(path, data.Location.Href)
	// 	switch data.Type {
	// 	case "other":
	// 		err = loadXML(pth, &other)
	// 	case "primary":
	// 		err = loadXML(pth, &primary)
	// 	case "filelists":
	// 		err = loadXML(pth, &fl)
	// 	}

	// 	if err != nil {
	// 		return
	// 	}
	// }

	repo = &Repository{Metadata: md, path: path}
	return
}

func (r *Repository) loadData(dtype string, v interface{}) (err error) {
	if dtype != "other" && dtype != "primary" && dtype != "filelists" {
		err = errors.New("Invalid data type; must be one of 'other', 'primary', or 'filelists'")
		return
	}

	for _, d := range r.Metadata.Data {
		if d.Type == dtype {
			dpath := filepath.Join(r.path, d.Location.Href)
			if err = loadXML(dpath, v); err != nil {
				return
			}
			break
		}
	}

	return
}

func (r *Repository) LoadFilelists() (fl yumfl.Filelists, err error) {
	err = r.loadData("filelists", &fl)
	return
}

func (r *Repository) LoadOther() (o yumother.Otherdata, err error) {
	err = r.loadData("other", &o)
	return
}

func (r *Repository) LoadPrimary() (p yumprimary.Primary, err error) {
	err = r.loadData("primary", &p)
	return
}
