package filelists

import "encoding/xml"

type Filelists struct {
	XMLName  xml.Name  `xml:"filelists"`
	Packages int       `xml:"packages,attr"`
	Package  []Package `xml:"package"`
}

type Package struct {
	XMLName xml.Name       `xml:"package"`
	PkgId   string         `xml:"pkgid,attr"`
	Name    string         `xml:"name,attr"`
	Arch    string         `xml:"arch,attr"`
	Version PackageVersion `xml:"version"`
	File    []File         `xml:"file"`
}

type File struct {
	XMLName xml.Name `xml:"file"`
	Path    string   `xml:",chardata"`
	Type    string   `xml:"type,attr,omitempty"`
}

type PackageVersion struct {
	XMLName xml.Name `xml:"version"`
	Epoch   int64    `xml:"epoch,attr"`
	Ver     string   `xml:"ver,attr"`
	Rel     string   `xml:"rel,attr"`
}
