package goyum

import "encoding/xml"

type Otherdata struct {
	XMLName  xml.Name
	Packages int       `xml:"packages,attr"`
	Package  []Package `xml:"package"`
}

type Package struct {
	XMLName xml.Name
	PkgId   string         `xml:"pkgid,attr"`
	Name    string         `xml:"name,attr"`
	Arch    string         `xml:"arch,attr"`
	Version PackageVersion `xml:"version"`
}

type Changelog struct {
	XMLName xml.Name
	Author  string `xml:"author,attr"`
	Date    int64  `xml:"date,attr"`
	Log     string `xml:",chardata"`
}

type PackageVersion struct {
	XMLName xml.Name
	Epoch   int64  `xml:"epoch,attr"`
	Ver     string `xml:"ver,attr"`
	Rel     string `xml:"rel,attr"`
}
