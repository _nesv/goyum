package primary

import "encoding/xml"

type Primary struct {
	XMLName  xml.Name  `xml:"metadata"`
	Packages int       `xml:"packages,attr"`
	Package  []Package `xml:"package"`
}

type Package struct {
	XMLName     xml.Name `xml:"package"`
	Type        string   `xml:"type,attr"`
	Name        string   `xml:"name"`
	Arch        string   `xml:"arch"`
	Version     PackageVersion
	Checksum    PackageChecksum
	Summary     string `xml:"summary"`
	Description string `xml:"description"`
	Packager    string `xml:"packager"`
	URL         string `xml:"url"`
	Time        PackageTime
	Size        PackageSize
	Location    PackageLocation
	Format      PackageFormat
}

type PackageVersion struct {
	XMLName xml.Name `xml:"version"`
	Epoch   int64    `xml:"epoch,attr"`
	Ver     string   `xml:"ver,attr"`
	Rel     string   `xml:"rel,attr"`
}

type PackageChecksum struct {
	XMLName  xml.Name `xml:"checksum"`
	Type     string   `xml:"type,attr"`
	PkgId    string   `xml:"pkgid,attr"`
	Checksum string   `xml:",chardata"`
}

type PackageTime struct {
	XMLName xml.Name `xml:"time"`
	File    int64    `xml:"file,attr"`
	Build   int64    `xml:"build,attr"`
}

type PackageSize struct {
	XMLName   xml.Name `xml:"size"`
	Package   int      `xml:"package,attr"`
	Installed int      `xml:"installed,attr"`
	Archive   int      `xml:"archive,attr"`
}

type PackageLocation struct {
	XMLName xml.Name `xml:"location"`
	Href    string   `xml:"href,attr"`
}

type PackageFormat struct {
	XMLName     xml.Name `xml:"format"`
	License     string   `xml:"http://linux.duke.edu/metadata/rpm license"`
	Vendor      string   `xml:"http://linux.duke.edu/metadata/rpm vendor"`
	Group       string   `xml:"http://linux.duke.edu/metadata/rpm group"`
	BuildHost   string   `xml:"http://linux.duke.edu/metadata/rpm buildhost"`
	SourceRpm   string   `xml:"http://linux.duke.edu/metadata/rpm sourcerpm"`
	HeaderRange PackageFormatHeaderRange
	Provides    PackageFormatProvides `xml:"http://linux.duke.edu/metadata/rpm provides,omitempty"`
	Requires    PackageFormatRequires `xml:"http://linux.duke.edu/metadata/rpm requires,omitempty"`
	Files       []PackageFormatFile   `xml:"file,omitempty"`
}

type PackageFormatLicense struct {
	Data string `xml:",chardata"`
}

type PackageFormatHeaderRange struct {
	XMLName xml.Name `xml:"http://linux.duke.edu/metadata/rpm header-range"`
	Start   int      `xml:"start,attr"`
	End     int      `xml:"end,attr"`
}

type PackageFormatProvides struct {
	XMLName xml.Name             `xml:"http://linux.duke.edu/metadata/rpm provides"`
	Entries []PackageFormatEntry `xml:"http://linux.duke.edu/metadata/rpm entry,omitempty"`
}

type PackageFormatRequires struct {
	XMLName xml.Name             `xml:"http://linux.duke.edu/metadata/rpm requires"`
	Entries []PackageFormatEntry `xml:"http://linux.duke.edu/metadata/rpm entry,omitempty"`
}

type PackageFormatEntry struct {
	XMLName xml.Name `xml:"http://linux.duke.edu/metadata/rpm entry"`
	Name    string   `xml:"name,attr"`
	Flags   string   `xml:"flags,attr,omitempty"`
	Epoch   int64    `xml:"epoch,attr,omitempty"`
	Ver     string   `xml:"ver,attr,omitempty"`
	Rel     string   `xml:"rel,attr,omitempty"`
}

type PackageFormatFile struct {
	XMLName xml.Name `xml:"file"`
	Path    string   `xml:",chardata"`
	Type    string   `xml:"type,attr,omitempty"`
}
