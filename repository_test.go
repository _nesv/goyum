package goyum

import "testing"

var r *Repository

func TestLoadRepo(t *testing.T) {
	var err error
	r, err = LoadRepository("testdata")
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%#v", r)
}

func TestRepoLoadFilelists(t *testing.T) {
	fl, err := r.LoadFilelists()
	if err != nil {
		t.Error(err)
	}
	t.Logf("%#v", fl.Package[0])
}

func TestRepoLoadOther(t *testing.T) {
	o, err := r.LoadOther()
	if err != nil {
		t.Error(err)
	}
	t.Logf("%#v", o.Package[0])
}

func TestRepoLoadPrimary(t *testing.T) {
	p, err := r.LoadPrimary()
	if err != nil {
		t.Error(err)
	}
	if len(p.Package) > 0 {
		pkg := p.Package[0]
		t.Logf("%#v", pkg)
	}
}
