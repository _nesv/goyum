package goyum

import (
	"compress/gzip"
	"encoding/xml"
	"io/ioutil"
	"os"
	"path/filepath"
)

func loadXML(filename string, v interface{}) (err error) {
	// Open the file.
	var f *os.File
	if f, err = os.Open(filename); err != nil {
		return
	}
	defer f.Close()

	// We will have to do a bit of switching here, depending on whether
	// or not the file is compressed. For this, we are just going to check
	// the file's extension.
	var data []byte
	switch filepath.Ext(filename) {
	case ".gz":
		var gzr *gzip.Reader
		if gzr, err = gzip.NewReader(f); err != nil {
			return
		}

		if data, err = ioutil.ReadAll(gzr); err != nil {
			return
		}

	default:
		if data, err = ioutil.ReadAll(f); err != nil {
			return
		}
	}

	err = xml.Unmarshal(data, v)
	return
}
